class CreateTransations < ActiveRecord::Migration
  def change
    create_table :transations do |t|
      t.decimal  "accounts_payable"
      t.decimal  "accounts_receivable"
      t.string   "memo"
      t.datetime "spend_date"
      t.timestamps
    end
  end
end
